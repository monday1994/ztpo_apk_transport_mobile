package com.example.delivery.deliveryapp.model;

/**
 * Created by AdelaK on 2017-05-13.
 */

public class DriverModel {

    private String name;
    private String lastName;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
       return String.format("%s %s",
               this.name, this.lastName);
    }
}
