package com.example.delivery.deliveryapp.utils;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.delivery.deliveryapp.R;
import com.example.delivery.deliveryapp.model.ProductModel;
import com.example.delivery.deliveryapp.remote.Http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by AdelaK on 2017-05-15.
 */

public class DriversFragment extends Fragment {


    private View view;
    private ListView listView;
    String response;
    private List productsList;
    private ArrayAdapter<ProductModel> adapter;
    private SharedPreferences userLoginPrefs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_drivers_list, container, false);

        String result = getListOfProducts();
        try {
            productsList = getListOfProductsModel(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new ArrayAdapter<ProductModel>(view.getContext(), android.R.layout.simple_list_item_1, productsList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                 ProductModel product = (ProductModel) listView.getItemAtPosition(position);
                final ArrayList<String> ids = new ArrayList<String>();
                ids.add(product.getId());
                new AlertDialog.Builder(getActivity())
                        .setTitle("Remove package")
                        .setMessage("Are you sure you want to delete this package?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteProduct(ids);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
        return view;
    }

    private void deleteProduct(final ArrayList<String> product) {
        final Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put(getString(R.string.email), getEmail());
        parameterMap.put(getString(R.string.packages_Id), product.toString());
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Post(getString(R.string.delete_package),parameterMap);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    public String getListOfProducts() {
        response = "";
        final Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put(getString(R.string.dEmail), getEmail());
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Get(getString(R.string.get_packages_for_driver_url),parameterMap);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    private List getListOfProductsModel(String result) throws JSONException {
        JSONObject json = new JSONObject(result);
        String listOfProducts = json.getString("packages").toString();
        JSONArray jsonArray = new JSONArray(listOfProducts);
        List productList = new ArrayList<ProductModel>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            ProductModel product = new ProductModel();
            try {
                product.setId(jsonObject.get("_id").toString());
                product.setReceiverAddress(jsonObject.get("receiverAddress").toString());
                product.setReceiverPostalCode(jsonObject.get("receiverPostalCode").toString());
                product.setWeight(jsonObject.get("weight").toString());
                productList.add(product);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return productList;
    }

    public String getEmail() {
        userLoginPrefs = view.getContext().getSharedPreferences("login_preference", Context.MODE_PRIVATE);
        return userLoginPrefs.getString(getString(R.string.login), null);
    }

}
