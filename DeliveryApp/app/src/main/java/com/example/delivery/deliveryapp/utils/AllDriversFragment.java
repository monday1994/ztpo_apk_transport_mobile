package com.example.delivery.deliveryapp.utils;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.RecoverySystem;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;

import com.example.delivery.deliveryapp.R;
import com.example.delivery.deliveryapp.model.DriverModel;
import com.example.delivery.deliveryapp.model.ProductModel;
import com.example.delivery.deliveryapp.remote.Http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by AdelaK on 2017-05-13.
 */

public class AllDriversFragment extends Fragment {


    private View view;
    private List driversList;
    private ArrayAdapter<DriverModel> adapter;
    String response;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_drivers_list, container, false);
        String result = getList(getString(R.string.get_drivers_url));
        List<DriverModel> drivers;
        try {
            driversList = getListOfDrivers(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ListView listView1 = (ListView) view.findViewById(R.id.listView);
        adapter = new ArrayAdapter<DriverModel>(view.getContext(), android.R.layout.simple_list_item_1, driversList);
        listView1.setAdapter(adapter);
        return view;
    }

    private String getList(final String url) {
        response = "";
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Get(url,null);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    private List<DriverModel> getListOfDrivers(String result) throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        List driverList = new ArrayList<DriverModel>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            DriverModel driver = new DriverModel();
            try {
                driver.setEmail(jsonObject.get("email").toString());
                driver.setName(jsonObject.get("name").toString());
                driver.setLastName(jsonObject.get("lastName").toString());
                driverList.add(driver);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return driverList;
    }


}


